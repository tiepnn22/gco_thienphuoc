$(document).ready(function() {
    $().fancybox({
        thumbs: true,
        hash: false,
        loop: true,
    });

    MobileMenu.init();
    css_variations_woo();
    
    // Change select attribute to radio woocommerce quickview
    $(document).on('woosq_loaded', function() {
        css_variations_woo();
    });
});

// Change select attribute to radio woocommerce
function css_variations_woo() {
    $( "label.generatedRadios" ).remove();
    $( "table.variations select" ).each( function() {
        var selName = $( this ).attr( "name" );
        $( "select[name=" + selName + "] option" ).each( function() {
            var option = $( this );
            var value = option.attr( "value" );
            if( value == "" ) { return; }
            var label = option.html();
            var select = option.parent();
            var selected = select.val();
            var isSelected = ( selected == value )
                ? " checked=\"checked\"" : "";
            var selClass = ( selected == value )
                ? " selected" : "";
            var radHtml
                = `<input name="${selName}" ${isSelected} type="radio" value="${value}" />`;
            var optionHtml
                = `<label class="generatedRadios${selClass}">${radHtml} ${label}</label>`;
            select.parent().append(
                $( optionHtml ).click( function() {
                    select.val( value ).trigger( "change" );
                } )
            )
        } ).parent().hide();
    } );
}

var MobileMenu = function () {
    var _initInstances = function () {

        var mobileMenu = $('[data-menu]');
        if (mobileMenu.length) {
            mobileMenu.each(function () {
                var el = $(this).data('menu');

                $(el).mmenu({
                    "extensions": [
                        // "fx-panels-zoom",
                        "pagedim-black",
                        // "theme-dark"
                    ],
                    "offCanvas": {
                        // "position": "right"
                    },
                    // "navbars": [
                    //     {
                    //         "position": "bottom",
                    //         "content": [
                    //             "<a class='fa fa-envelope' href='#/'></a>",
                    //             "<a class='fa fa-twitter' href='#/'></a>",
                    //             "<a class='fa fa-facebook' href='#/'></a>"
                    //         ]
                    //     }
                    // ]
                });
            })
        }
    };
    return {
        init: function () {
            _initInstances();
        }
    };
}();

// click cart
    $(document).ready(function() {
        $('.hd-cart').click(function(){
            $('.quickview-cart').stop(true,false).fadeToggle(); 
        });
        // menu pc
        $('header .nav-bar .list-menu ul li').addClass('menu-item');

        // $('#NavDrawer .inline-list > ul').addClass('mobile-nav');
        // $('#NavDrawer .inline-list ul li').addClass('mobile-nav__item');

        // click sidebar product
        $('.collection-sidebar .widget > div > *').addClass('panel');
        $('.collection-sidebar .widget > div > button').removeClass('panel');
        $('button.accordion').click(function(){
            $(this).parent().find('.panel').stop(true,false).slideToggle(); 
        });
    });

// click search
    function showSearch() {
        var box1 = document.querySelector('.search-form-wrapper');
        var outside1 = function(event) {
            if (!box1.contains(event.target)) {
                $(".search-form-wrapper").removeClass('active');
                // $('#searchtext').val('');
                // $('#search_smart #product').html('');
                // $('#search_smart #article').html('');
                this.removeEventListener(event.type, outside1);
            }
        }
        document.querySelector('#show_search_smart').addEventListener('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            $(".search-form-wrapper").toggleClass('active');
            document.addEventListener('click', outside1);
        });
    }
    showSearch();

// tab
    function openTab(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    if ($('#defaultOpen').length > 0) {
        document.getElementById("defaultOpen").click();
    }

    // 2
    function openProTabs(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("pro-tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("pro-tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    if ($('#defaultOpenProTabs').length > 0) {
        document.getElementById("defaultOpenProTabs").click();
    }

// Owl carousel init
    $(document).ready(function() {
        $('.header-search a').click(function() {
            $('.hd-search-wrapper').toggle();
        });
        $('.header-mobi-form a').click(function() {
            $('.hd-search-wrapper').toggle();
        });
        $('.open-search').click(function() {
            $('#searchauto').toggle();
        });

        $("#main-slider").owlCarousel({
            items: 1,
            itemsDesktop: [1000, 1],
            itemsDesktopSmall: [900, 1],
            itemsTablet: [600, 1],
            itemsMobile: [480, 1],
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            //autoPlay: 3000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#list-collection-slider").owlCarousel({
            items: 5,
            itemsDesktop: [1000, 5],
            itemsDesktopSmall: [900, 3],
            itemsTablet: [600, 3],
            itemsMobile: [480, 2],
            navigation: false,
            pagination: false,
            slideSpeed: 1000,
            //autoPlay: 3000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#home-article-slider").owlCarousel({
            items: 3,
            itemsDesktop: [1000, 3],
            itemsDesktopSmall: [900, 3],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: false,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#home-collection1-slider").owlCarousel({
            items: 4,
            itemsDesktop: [1000, 4],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 2],
            itemsMobile: [480, 1],
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            //autoPlay: 3000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#home-review-slider").owlCarousel({
            items: 1,
            itemsDesktop: [1000, 1],
            itemsDesktopSmall: [900, 1],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#home-partners-slider").owlCarousel({
            items: 4,
            itemsDesktop: [1000, 4],
            itemsDesktopSmall: [900, 3],
            itemsTablet: [600, 2],
            itemsMobile: false,
            navigation: false,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            //autoPlay: 3000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#home-collection-sale-slide").owlCarousel({
            items: 4,
            itemsDesktop: [1000, 4],
            itemsDesktopSmall: [900, 3],
            itemsTablet: [600, 3],
            itemsMobile: [480, 2],
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            //autoPlay: 3000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#related-product-slider").owlCarousel({
            items: 4,
            itemsDesktop: [1000, 4],
            itemsDesktopSmall: [900, 3],
            itemsTablet: [600, 3],
            itemsMobile: [480, 2],
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            //autoPlay: 3000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
        $("#owl-blog-single-slider1").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#owl-blog-single-slider2").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#owl-blog-single-slider3").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#owl-blog-single-slider4").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#owl-blog-single-slider5").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#owl-blog-single-slider6").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#owl-blog-single-slider7").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#owl-blog-single-slider8").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#owl-blog-single-slider9").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        $("#owl-blog-single-slider10").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    });