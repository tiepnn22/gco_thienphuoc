<?php get_header(); ?>

<?php
	$s = $_GET['s'];

	$page_name = __('Tìm kiếm', 'text_domain');

    //banner
    $data_page_banner  = array(
        'image_alt'    =>    $page_name
    );
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">
        <section id="blog-wrapper">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">

                        <div class="grid__item large--nine-twelfths medium--one-whole small--one-whole pull-right">
                            <div class="blog-content">
                                <div class="blog-content-wrapper">
                                    <div class="blog-head">
                                        <div class="blog-title">
                                            <h1>Tin tức</h1>
                                        </div>
                                    </div>

                                    <div class="blog-body">
                                        <div class="grid-uniform">

                                            <?php
                                                $query = query_search_post_paged( $s, array('post'), -1 );
                                                // $max_num_pages = $query->max_num_pages;

                                                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                                            ?>

                                                <?php get_template_part('resources/views/content/category-post', get_post_format()); ?>

                                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php get_sidebar(); ?>

                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">
        <section id="collection-wrapper" class="mg-bottom-30 mg-top-30">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">

                        <div class="grid__item large--three-twelfths medium--one-whole small--one-whole">
                            <!-- <div class="collection-sidebar">
                                <div class="grid-uniform">
                                </div>
                            </div> -->
                        </div>

                        <div class="grid__item large--nine-twelfths medium--one-whole small--one-whole mg-bottom-30">
                            <div class="collection-main">

                                <div id="page-wrapper" class="collection-title">
                                    <h1>Sản phẩm</h1>
                                </div>

                                <div class="collection-list collection-body">
                                    <div class="grid-uniform product-list md-mg-left-10">
                            
                                        <?php
                                            $query = query_search_post_paged( $s, array('product'), -1 );
                                            // $max_num_pages = $query->max_num_pages;

                                            if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                                        ?>

                                            <?php get_template_part('resources/views/content/category-product', get_post_format()); ?>

                                        <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<?php get_footer(); ?>