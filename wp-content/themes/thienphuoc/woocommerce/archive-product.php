<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' ); ?>

<?php
    $term_info         = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $term_id           = $term_info->term_id;
    $term_name_check   = $term_info->name;
    $term_name		   = (!empty($term_name_check)) ? $term_name_check : 'Sản phẩm';
    // $term_excerpt   = wpautop($term_info->description);
    // $term_link      = esc_url(get_term_link($term_id));
    // $taxonomy_name  = $term_info->taxonomy;
    // $thumbnail_id   = get_woocommerce_term_meta( $term_id, 'thumbnail_id', true );

    //banner
    // $data_page_banner  = array(
    //     'image_alt'    =>    $term_name
    // );
?>

<section id="breadcrumb-wrapper5" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
    <div class="breadcrumb-content">
        <div class="wrapper">
            <div class="inner text-center">
                <div class="breadcrumb-big">
                    <h2>
                        <?php echo $term_name;?>
                    </h2>
                </div>
                <div class="breadcrumb-small">
			        <?php
			            if(function_exists('bcn_display')) { 
			                echo '<a href="' . site_url() . '">Trang chủ </a> /';
			                bcn_display(); 
			            }
			        ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
// do_action( 'woocommerce_before_main_content' );
?>
<!-- <header class="woocommerce-products-header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

	<?php
		do_action( 'woocommerce_archive_description' );
	?>
</header> -->

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">
        <section id="collection-wrapper" class="mg-bottom-30 mg-top-30">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">

                    	<div class="grid__item large--nine-twelfths medium--one-whole small--one-whole mg-bottom-30">
                    		<div class="collection-main">

                    			<div class="collection-list collection-body">
                                    <div class="grid-uniform product-list md-mg-left-10">
							
										<?php
											if ( woocommerce_product_loop() ) {

												do_action( 'woocommerce_before_shop_loop' );

												// woocommerce_product_loop_start();
												if ( wc_get_loop_prop( 'total' ) ) {
													while ( have_posts() ) {
														the_post();

														// do_action( 'woocommerce_shop_loop' );
														// wc_get_template_part( 'content', 'product' );
														get_template_part('resources/views/content/category-product', get_post_format());
													}
												}
												// woocommerce_product_loop_end();

												do_action( 'woocommerce_after_shop_loop' );
											} else {
												do_action( 'woocommerce_no_products_found' );
											}
										?>

                                    </div>
                                </div>

                            </div>
						</div>

						<div class="grid__item large--three-twelfths medium--one-whole small--one-whole">
                            <div class="collection-sidebar">
                                <div class="grid-uniform">
                                	<?php dynamic_sidebar( 'sidebar-product' ); ?>
                                </div>
                            </div>
                        </div>

					</div>
				</div>
			</div>
		</section>
	</main>
</div>

<?php
// do_action( 'woocommerce_after_main_content' );

get_footer( 'shop' );
