<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<?php
    $product_id = get_the_ID();
    $single_product_image   = getPostImage($product_id,"full");

    // $terms = wp_get_object_terms($post->ID, 'product_cat');
    // if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];

    $term_brand = wp_get_post_terms($product_id,'product_brand',array('fields'=>'ids'));
    $term_types = wp_get_post_terms($product_id,'product_types',array('fields'=>'ids'));

    // woocommerce
    // $product = new WC_product($product_id);

    // wc gallery
    $single_product_gallery = $product->get_gallery_image_ids();

    // wc sku
    $product_sku = $product->get_sku();
?>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">
        <div class="product-wapper mg-bottom-30">
            <div class="wrapper">
            	<div class="inner">

                    
					<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
                        <div class="grid product-single">
    						
                            <div class="grid__item large--one-half medium--one-whole small--one-whole">
                                <div class="product-single__photos">
                                    <div id="ProductPhoto" class="owl-carousel owl-enable" data-slide="1" data-pagination="false" data-nav="false">
                                        <div class="item">
                                            <a>
                                                <img class="lazyload" src="" data-src="<?php echo $single_product_image; ?>" />
                                            </a>
                                        </div>

                                        <?php if(!empty( $single_product_gallery )) { ?>
                                        <?php
                                            foreach( $single_product_gallery as $single_product_gallery_kq ){

                                            $post_image = wp_get_attachment_url( $single_product_gallery_kq );
                                        ?>
                                        <div class="item">
                                            <a>
                                                <img class="lazyload" src="" data-src="<?php echo $post_image; ?>" />
                                            </a>
                                        </div>
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <ul class="product-single__thumbnails inline-list" id="ProductThumbs" data-nav="false">
                                    <div class="inner">
                                        <li class="thumbnail-item">
                                            <a href="<?php echo $single_product_image; ?>" class="product-single__thumbnail" data-pos="0">
                                                <img class="lazyload" src="" data-src="<?php echo $single_product_image; ?>">
                                            </a>
                                        </li>

                                        <?php if(!empty( $single_product_gallery )) { ?>
                                        <?php
                                            $i = 1;
                                            foreach( $single_product_gallery as $single_product_gallery_kq ){

                                            $post_image = wp_get_attachment_url( $single_product_gallery_kq );
                                        ?>
                                        <li class="thumbnail-item">
                                            <a href="<?php echo $post_image; ?>" class="product-single__thumbnail" data-pos="<?php echo $i; ?>">
                                                <img class="lazyload" src="" data-src="<?php echo $post_image; ?>">
                                            </a>
                                        </li>
                                        <?php $i++; } ?>
                                        <?php } ?>
                                    </div>
                                    <div class="product-thumb-control medium--hide small--hide">
                                        <button class="btn up">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                        </button>
                                        <button class="btn down">
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </ul>
                                <?php
        							// do_action( 'woocommerce_before_single_product_summary' ); // gallery
        						?>
                            </div>

                            <div class="grid__item large--one-half product-page">
        						<!-- <div class="summary entry-summary"> -->
        							<?php
        								do_action( 'woocommerce_single_product_summary' ); // info product
        							?>
        						<!-- </div> -->
                                <div class="pro-brand">
                                    <span class="title">
                                        <strong>Thương hiệu:</strong> 
                                        <?php if(!empty( $term_brand )) { ?>
                                        <?php
                                            foreach ($term_brand as $foreach_kq) {

                                            $post_id      = $foreach_kq;
                                            $post_name    = get_term_by( 'id', $post_id, 'product_brand' )->name;
                                            $post_link    = esc_url(get_term_link($post_id));
                                        ?>
                                            <a href="<?php echo $post_link; ?>"><?php echo $post_name; ?></a>
                                        <?php } ?>
                                        <?php } ?>
                                    </span>
                                </div>
                                <div class="pro-type">
                                    <span class="title">
                                        <strong>Loại:</strong> 
                                        <?php if(!empty( $term_types )) { ?>
                                        <?php
                                            foreach ($term_types as $foreach_kq) {

                                            $post_id      = $foreach_kq;
                                            $post_name    = get_term_by( 'id', $post_id, 'product_types' )->name;
                                            $post_link    = esc_url(get_term_link($post_id));
                                        ?>
                                            <a href="<?php echo $post_link; ?>"><?php echo $post_name; ?></a>
                                        <?php } ?>
                                        <?php } ?>
                                    </span>
                                </div>
                                <span class="product-more-info">
                                    <span class="product-sku">
                                        <strong>MSP : </strong> 
                                        <span id="ProductSku" class="ProductSku" itemprop="SKU"><?php echo $product_sku; ?></span>
                                    </span>
                                </span>
                                <div class="share-social text-left">
                                    <?php get_template_part("resources/views/socical-bar"); ?>
                                    <div class="fb-like" data-href="<?php the_permalink(); ?>" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="true"></div>
                                </div>
                            </div>

    						<?php
    							// do_action( 'woocommerce_after_single_product_summary' ); // tab, related product
    						?>

    					</div>
				    </div>

                    <div class="product-tabs mg-bottom-30">
                        <div class="tab clearfix">
                            <button class="pro-tablinks" onclick="openProTabs(event, 'protab0')" id="defaultOpenProTabs"><span>Mô tả</span></button>
                            <!-- <button class="pro-tablinks" onclick="openProTabs(event, 'protab1')"><span>Hướng dẫn</span></button> -->
                            <!-- <button class="pro-tablinks" onclick="openProTabs(event, 'protab2')"><span>Bảo hành</span></button> -->
                            <button class="pro-tablinks" onclick="openProTabs(event, 'protab3')"><span>Bình luận</span></button>
                        </div>
                        <div id="protab0" class="pro-tabcontent">
                            <p><?php echo wpautop(get_the_content( get_the_ID() )); ?></p>
                        </div>
                        <!-- <div id="protab1" class="pro-tabcontent">
                        	1
                        </div>
                        <div id="protab2" class="pro-tabcontent">
                        	2
                        </div> -->
                        <div id="protab3" class="pro-tabcontent">
                            <div class="product-fb-comments">
                                <!-- <div class="fb-comments" data-href="https://suplo-noi-that.myharavan.com/products/ban-tron-mot-chan" data-width="100%" data-numposts="5"></div> -->
                                <?php
                                    if ( comments_open() || get_comments_number() ) {
                                        comments_template();
                                    }
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php get_template_part("resources/views/template-related-product"); ?>

                </div>
			</div>
		</div>
	</main>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
