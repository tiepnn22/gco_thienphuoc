<?php
	/*
	Template Name: Mẫu Hệ thống cửa hàng
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //banner
    // $page_banner_check = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full');
    // $page_banner       = (!empty($page_banner_check[0])) ? $page_banner_check[0] : '';
    $data_page_banner  = array(
        // 'image_link'   =>    $page_banner, 
        'image_alt'    =>    $page_name
    );

    $store_address    = get_field('store_address');
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">
        <section id="page-wrapper">
            <div class="wrapper">
                <div class="inner">
                    <div>
                        <div id="shopmap-page">

                            <h1><?php echo $page_name; ?></h1>
                            <div class="content-page">
                                <div class="page-contact-shoplist">
                                    <div class="pcontact-shoplist-wrapper">
                                        <div class="grid-uniform md-mg-left-15">

                                            <?php if(!empty( $store_address )) { ?>
                                            <?php
                                                foreach ($store_address as $foreach_kq) {

                                                $post_image = $foreach_kq["image"];
                                                $post_title = $foreach_kq["title"];
                                                $post_link  = $foreach_kq["url"];
                                                $post_phone = $foreach_kq["phone"];
                                            ?>
                                                <div class="grid__item md-pd-left15 large--one-third medium--one-third small--one-half">
                                                    <div class="pcontact-shop-item">
                                                        <div class="pcontact-shop-img">
                                                            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" />
                                                        </div>
                                                        <div class="pcontact-shop-address">
                                                            <a href="<?php echo $post_link; ?>" target="_blank">
                                                                <i class="fa fa-map-marker"></i> <?php echo $post_title; ?>
                                                            </a>
                                                        </div>
                                                        <div class="pcontact-shop-tel">
                                                            <a href="tel:<?php echo str_replace(' ','',$post_phone);?>">
                                                                <i class="fa fa-phone"></i> <?php echo $post_phone; ?>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<?php get_footer(); ?>