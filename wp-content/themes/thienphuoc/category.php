<?php get_header(); ?>

<?php
	$category_info 	= get_category_by_slug( get_query_var( 'category_name' ) );
	$cat_id 		= $category_info->term_id;
	$cat_name 		= get_cat_name($cat_id);
	$cat_excerpt 	= wpautop(category_description($cat_id));
	$cat_link 		= esc_url(get_term_link($cat_id));
	
	//banner
	// $page_banner 	   = get_field('page_banner', 'category_'.$cat_id);
	$data_page_banner  = array(
		// 'image_link'   =>    $page_banner,
		'image_alt'    =>    $cat_name
	);
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">
        <section id="blog-wrapper">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">

                        <div class="grid__item large--nine-twelfths medium--one-whole small--one-whole pull-right">
                            <div class="blog-content">
                                <div class="blog-content-wrapper">
                                    <div class="blog-head">
                                        <div class="blog-title">
                                            <h1><?php echo $page_name; ?></h1>
                                        </div>
                                    </div>

                                    <div class="blog-body">
                                        <div class="grid-uniform">

                                            <?php
                                                $query = query_post_by_category_paged($cat_id, 4);
                                                $max_num_pages = $query->max_num_pages;

                                                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                                            ?>

                                                <?php get_template_part('resources/views/content/category-post', get_post_format()); ?>

                                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                                        </div>
                                    </div>

                                    <nav class="vk-pagination">
                                        <?php echo paginationCustom( $max_num_pages ); ?>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <?php get_sidebar(); ?>

                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<?php get_footer(); ?>