<?php
    //field
    $customer_address   = get_field('customer_address', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_email     = get_field('customer_email', 'option');

    $f_subscribe_title    = get_field('f_subscribe_title', 'option');
    $f_subscribe_form_id  = get_field('f_subscribe_form', 'option');
    $f_subscribe_form     = do_shortcode('[contact-form-7 id="'.$f_subscribe_form_id.'"]');

    $f_socical_facebook   = get_field('f_socical_facebook', 'option');
    $f_socical_insta      = get_field('f_socical_insta', 'option');
    $f_socical_youtube    = get_field('f_socical_youtube', 'option');
    $f_socical_twitter    = get_field('f_socical_twitter', 'option');
    $f_socical_google     = get_field('f_socical_google', 'option');

    $f_bottom_copyright   = get_field('f_bottom_copyright', 'option');
?>

<footer id="footer">
    <div class="footer-top">
        <div class="wrapper">
            <div class="inner">
                <div class="footer-phone-contact">
                    <div class="footer-phone-title">Gọi chúng tôi bất cứ khi nào bạn cần</div>
                    <a class="footer-phone-number" href="tel:<?php echo str_replace(' ','',$customer_phone);?>">
                        <?php echo $customer_phone; ?>
                    </a>
                </div>
                <div class="footer-main">
                    <div class="grid">
                        <div class="grid__item large--one-third medium--one-half small--one-whole mg-bottom-30">
                            <div class="footer-info">
                                <h3 class="footer-title">Hệ thống cửa hàng nội thất</h3>
                                <div class="ft-address">
                                    <i class="fa fa-home"></i>
                                    <span><?php echo $customer_address; ?></span>
                                </div>
                                <div class="ft-hotline">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>"><?php echo $customer_phone; ?></a>
                                </div>
                                <div class="ft-email">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <a href="mailto:<?php echo $customer_email; ?>"><?php echo $customer_email; ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="grid__item large--one-third medium--one-half small--one-whole mg-bottom-30">
                            <div class="footer-menu">
                                <h3 class="footer-title">Dịch vụ</h3>
                                <?php
                                    if(function_exists('wp_nav_menu')){
                                        $args = array(
                                            'theme_location'    =>  'service',
                                            'container_class'   =>  '',
                                            'menu_class'        =>  'footer-nav-list no-bullets'
                                        );
                                        wp_nav_menu( $args );
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="grid__item large--one-third medium--one-half small--one-whole mg-bottom-30">
                            <div class="footer-contact">
                                <h3 class="footer-title"><?php echo $f_subscribe_title; ?></h3>
                                <div class="contact-form-wrapper">
                                    <div class="form-vertical clearfix">
                                        <?php if(!empty( $f_subscribe_form )) { ?>
                                        <div class='contact-form'>
                                            <?php echo $f_subscribe_form; ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="wrapper">
            <div class="grid">
                <div class="grid__item large--one-half medium--one-half small--one-whole">
                    <div class="copyright"><?php echo $f_bottom_copyright; ?></div>
                </div>
                <div class="grid__item large--one-half medium--one-half small--one-whole">
                    <div class="footer-social">
                        <div class="list-social-network">
                            <a href="<?php echo $f_socical_facebook; ?>" class="social-icon fb-icon" target="_blank">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                            <a href="<?php echo $f_socical_insta; ?>" class="social-icon ins-icon" target="_blank">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                            <a href="<?php echo $f_socical_youtube; ?>" class="social-icon yt-icon" target="_blank">
                                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                            </a>
                            <a href="<?php echo $f_socical_twitter; ?>" class="social-icon tw-icon" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                            <a href="<?php echo $f_socical_google; ?>" class="social-icon gp-icon" target="_blank">
                                <i class="fa fa-google-plus" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/api.jquery.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/option_selection.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/fastclick.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/script.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/timber.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/carousel.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/fancybox.min.css">
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/fancybox.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/handlebars.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/ajax-cart.js"></script> -->

<!-- <script src='http://hstatic.net/0/0/global/api.jquery.js' type='text/javascript'></script> -->
<!-- <script src='http://hstatic.net/0/0/global/option_selection.js' type='text/javascript'></script> -->
<!-- <script src='http://theme.hstatic.net/1000256920/1000358817/14/fastclick.min.js?v=317' type='text/javascript'></script> -->
<!-- <script src='http://theme.hstatic.net/1000256920/1000358817/14/script.js?v=317' type='text/javascript'></script> -->
<!-- <script src='http://theme.hstatic.net/1000256920/1000358817/14/timber.js?v=317' type='text/javascript'></script> -->
<!-- <script src='http://theme.hstatic.net/1000256920/1000358817/14/owl.carousel.min.js?v=317' type='text/javascript'></script> -->

<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css" /> -->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script> -->
<!-- <script src='http://theme.hstatic.net/1000256920/1000358817/14/handlebars.min.js?v=317' type='text/javascript'></script> -->
<!-- <script src='http://theme.hstatic.net/1000256920/1000358817/14/ajax-cart.js?v=317' type='text/javascript'></script> -->

<script>
    // $(document).ready(function() {
    //     $().fancybox({
    //         thumbs: true,
    //         hash: false,
    //         loop: true,
    //     });
    // });
</script>
<!-- click cart -->
<script>
    // $(document).ready(function() {
    //     $('.hd-cart').click(function(){
    //         $('.quickview-cart').stop(true,false).fadeToggle(); 
    //     });
    //     // menu pc
    //     $('header .nav-bar .list-menu ul li').addClass('menu-item');

    //     // $('#NavDrawer .inline-list > ul').addClass('mobile-nav');
    //     // $('#NavDrawer .inline-list ul li').addClass('mobile-nav__item');

    //     // click sidebar product
    //     $('.collection-sidebar .widget > div > *').addClass('panel');
    //     $('.collection-sidebar .widget > div > button').removeClass('panel');
    //     $('button.accordion').click(function(){
    //         $(this).parent().find('.panel').stop(true,false).slideToggle(); 
    //     });
    // });
</script>
<!-- click search -->
<script>
    // function showSearch() {
    //     var box1 = document.querySelector('.search-form-wrapper');
    //     var outside1 = function(event) {
    //         if (!box1.contains(event.target)) {
    //             $(".search-form-wrapper").removeClass('active');
    //             // $('#searchtext').val('');
    //             // $('#search_smart #product').html('');
    //             // $('#search_smart #article').html('');
    //             this.removeEventListener(event.type, outside1);
    //         }
    //     }
    //     document.querySelector('#show_search_smart').addEventListener('click', function(event) {
    //         event.preventDefault();
    //         event.stopPropagation();
    //         $(".search-form-wrapper").toggleClass('active');
    //         document.addEventListener('click', outside1);
    //     });
    // }
    // showSearch();
</script>
<!-- accordion -->
<script>
    // var acc = document.getElementsByClassName("accordion");
    // var i;

    // for (i = 0; i < acc.length; i++) {
    //     acc[i].onclick = function() {
    //         this.classList.toggle("active");
    //         var panel = this.nextElementSibling;
    //         if (panel.style.maxHeight) {
    //             panel.style.maxHeight = null;
    //         } else {
    //             panel.style.maxHeight = panel.scrollHeight + "px";
    //         }
    //     }
    // }

    // if ($(window).width() > 769) {
    //     $('.accordion.col-sb-trigger').trigger('click');
    // }
</script>
<!-- tab -->
<script>
 //    function openTab(evt, cityName) {
 //        var i, tabcontent, tablinks;
 //        tabcontent = document.getElementsByClassName("tabcontent");
 //        for (i = 0; i < tabcontent.length; i++) {
 //            tabcontent[i].style.display = "none";
 //        }
 //        tablinks = document.getElementsByClassName("tablinks");
 //        for (i = 0; i < tablinks.length; i++) {
 //            tablinks[i].className = tablinks[i].className.replace(" active", "");
 //        }
 //        document.getElementById(cityName).style.display = "block";
 //        evt.currentTarget.className += " active";
 //    }

 //    if ($('#defaultOpen').length > 0) {
 //    	document.getElementById("defaultOpen").click();
	// }

 //    // 2
 //    function openProTabs(evt, cityName) {
 //        var i, tabcontent, tablinks;
 //        tabcontent = document.getElementsByClassName("pro-tabcontent");
 //        for (i = 0; i < tabcontent.length; i++) {
 //            tabcontent[i].style.display = "none";
 //        }
 //        tablinks = document.getElementsByClassName("pro-tablinks");
 //        for (i = 0; i < tablinks.length; i++) {
 //            tablinks[i].className = tablinks[i].className.replace(" active", "");
 //        }
 //        document.getElementById(cityName).style.display = "block";
 //        evt.currentTarget.className += " active";
 //    }

 //    if ($('#defaultOpenProTabs').length > 0) {
 //        document.getElementById("defaultOpenProTabs").click();
 //    }
</script>
<!-- Owl carousel init -->
<script>
    // $(document).ready(function() {
    //     $('.header-search a').click(function() {
    //         $('.hd-search-wrapper').toggle();
    //     });
    //     $('.header-mobi-form a').click(function() {
    //         $('.hd-search-wrapper').toggle();
    //     });
    //     $('.open-search').click(function() {
    //         $('#searchauto').toggle();
    //     });

    //     $("#main-slider").owlCarousel({
    //         items: 1,
    //         itemsDesktop: [1000, 1],
    //         itemsDesktopSmall: [900, 1],
    //         itemsTablet: [600, 1],
    //         itemsMobile: [480, 1],
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         //autoPlay: 3000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#list-collection-slider").owlCarousel({
    //         items: 5,
    //         itemsDesktop: [1000, 5],
    //         itemsDesktopSmall: [900, 3],
    //         itemsTablet: [600, 3],
    //         itemsMobile: [480, 2],
    //         navigation: false,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         //autoPlay: 3000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#home-article-slider").owlCarousel({
    //         items: 3,
    //         itemsDesktop: [1000, 3],
    //         itemsDesktopSmall: [900, 3],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: false,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#home-collection1-slider").owlCarousel({
    //         items: 4,
    //         itemsDesktop: [1000, 4],
    //         itemsDesktopSmall: [900, 2],
    //         itemsTablet: [600, 2],
    //         itemsMobile: [480, 1],
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         //autoPlay: 3000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#home-review-slider").owlCarousel({
    //         items: 1,
    //         itemsDesktop: [1000, 1],
    //         itemsDesktopSmall: [900, 1],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#home-partners-slider").owlCarousel({
    //         items: 4,
    //         itemsDesktop: [1000, 4],
    //         itemsDesktopSmall: [900, 3],
    //         itemsTablet: [600, 2],
    //         itemsMobile: false,
    //         navigation: false,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         //autoPlay: 3000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#home-collection-sale-slide").owlCarousel({
    //         items: 4,
    //         itemsDesktop: [1000, 4],
    //         itemsDesktopSmall: [900, 3],
    //         itemsTablet: [600, 3],
    //         itemsMobile: [480, 2],
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         //autoPlay: 3000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#related-product-slider").owlCarousel({
    //         items: 4,
    //         itemsDesktop: [1000, 4],
    //         itemsDesktopSmall: [900, 3],
    //         itemsTablet: [600, 3],
    //         itemsMobile: [480, 2],
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         //autoPlay: 3000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });
    //     $("#owl-blog-single-slider1").owlCarousel({
    //         items: 2,
    //         itemsDesktop: [1000, 2],
    //         itemsDesktopSmall: [900, 2],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#owl-blog-single-slider2").owlCarousel({
    //         items: 2,
    //         itemsDesktop: [1000, 2],
    //         itemsDesktopSmall: [900, 2],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#owl-blog-single-slider3").owlCarousel({
    //         items: 2,
    //         itemsDesktop: [1000, 2],
    //         itemsDesktopSmall: [900, 2],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#owl-blog-single-slider4").owlCarousel({
    //         items: 2,
    //         itemsDesktop: [1000, 2],
    //         itemsDesktopSmall: [900, 2],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#owl-blog-single-slider5").owlCarousel({
    //         items: 2,
    //         itemsDesktop: [1000, 2],
    //         itemsDesktopSmall: [900, 2],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#owl-blog-single-slider6").owlCarousel({
    //         items: 2,
    //         itemsDesktop: [1000, 2],
    //         itemsDesktopSmall: [900, 2],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#owl-blog-single-slider7").owlCarousel({
    //         items: 2,
    //         itemsDesktop: [1000, 2],
    //         itemsDesktopSmall: [900, 2],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#owl-blog-single-slider8").owlCarousel({
    //         items: 2,
    //         itemsDesktop: [1000, 2],
    //         itemsDesktopSmall: [900, 2],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#owl-blog-single-slider9").owlCarousel({
    //         items: 2,
    //         itemsDesktop: [1000, 2],
    //         itemsDesktopSmall: [900, 2],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });

    //     $("#owl-blog-single-slider10").owlCarousel({
    //         items: 2,
    //         itemsDesktop: [1000, 2],
    //         itemsDesktopSmall: [900, 2],
    //         itemsTablet: [600, 1],
    //         itemsMobile: false,
    //         navigation: true,
    //         pagination: false,
    //         slideSpeed: 1000,
    //         paginationSpeed: 1000,
    //         navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    //     });
    // });
</script>
<!-- header scroll -->
<?php if(is_front_page()) { ?>
    <script>
        jQuery(document).ready(function() {
            window.onscroll = changePos;

            function changePos() {
                if ($(window).width() > 769) {
                    var header = $("#header .header-desktop");
                    var headerheight = $("#header .header-desktop").height();
                    
                    if (window.pageYOffset > headerheight) {
                        header.addClass('scroll-header');
                    } else {
                        header.removeClass('scroll-header');
                    }
                } else {
                    var header = $("#header");
                    var headerheight = $("#header").height();
                    if (window.pageYOffset > headerheight) {
                        header.addClass('scroll-header-navbar');
                    } else {
                        header.removeClass('scroll-header-navbar');
                    }
                }
            }
        });
    </script>
<?php } else { ?>
    <script>
        jQuery(document).ready(function() {
            window.onscroll = changePos;

            function changePos() {
                var header = $("#header");
                var headerheight = $("#header").height();
                
                if (window.pageYOffset > headerheight) {
                    header.addClass('scroll-header-navbar');
                } else {
                    header.removeClass('scroll-header-navbar');
                }
            }
        });
    </script>
<?php } ?>
<!-- Validate quantity form & trigger zoom -->
<script>
    // $("document").ready(function() {
    //     if ($(window).width() > 768) {
    //         setTimeout(function() {
    //             $("#ProductThumbs .thumbnail-item:first-child a").trigger('click');
    //         }, 3000);
    //     };

    //     $('#ProductThumbs .thumbnail-item').click(function() {
    //         $(this).addClass('border-red').siblings().removeClass('border-red');
    //     });

    // });
</script>

<script>
    // function Utils() {}
    // Utils.prototype = {
    //     constructor: Utils,
    //     isElementInView: function(element, fullyInView, pageTop) {
    //         var pageBottom = pageTop + $(window).height();
    //         if (element.length > 0) {
    //         	var elementTop = $(element).offset().top;
    //     	}
    //         var elementBottom = elementTop + $(element).height();	

    //         // if (fullyInView.length > 0) {
	   //          if (fullyInView === true) {
	   //              return ((pageTop < elementTop) && (pageBottom > elementBottom));
	   //          } else {
	   //              return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
	   //          }
	   //      // }
    //     }
    // };
    // var Utils = new Utils();
    // var isElementInView = Utils.isElementInView($('#home-review'), false, 0);
    // var counter = true;
    // $(window).scroll(function() {
    //     if (Utils.isElementInView($('#home-review'), false, $(window).scrollTop())) {
    //         if (counter) {
    //             $('.hr-statistic-number > span').each(function() {
    //                 var $this = $(this),
    //                     countTo = $this.attr('data-count');
    //                 $({
    //                     countNum: $this.text()
    //                 }).animate({
    //                     countNum: countTo
    //                 }, {
    //                     duration: 3000,
    //                     easing: 'linear',
    //                     step: function() {
    //                         $this.text(Math.floor(this.countNum));
    //                     },
    //                     complete: function() {
    //                         $this.text(this.countNum);
    //                     }
    //                 });
    //             });
    //             counter = false;
    //         }
    //     } else {}
    // });
</script>
<!-- date time -->
<!-- <script>
    // Set the date we're counting down to
    var countDownDate = new Date("07-29-2021 00:00:00").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        if ($('#days').length > 0) { document.getElementById("days").innerHTML = days; }
    	if ($('#hrs').length > 0) { document.getElementById("hrs").innerHTML = hours; }
        if ($('#mins').length > 0) { document.getElementById("mins").innerHTML = minutes; }
        if ($('#secs').length > 0) { document.getElementById("secs").innerHTML = seconds; }

        // If the count down is finished, write some text
        //if (distance < 0) {
        //      clearInterval(x);
        //    document.getElementById("flash-sale-status").innerHTML = "Flash Sale đã kết thúc";
        //}
    }, 1000);
</script> -->

<!-- quick view -->
<style>
    /*.modal {
        display: none;
        position: fixed;
        z-index: 99999;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: hidden;
        background-color: rgb(0, 0, 0);
        background-color: rgba(0, 0, 0, 0.4);
        transtion: all .5s;
    }

    .modal-content {
        background-color: #fefefe;
        margin: 5% auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
        max-width: 780px;
        transform: translatey(-30px);
        transition: all .5s;
    }

    .close {
        color: #aaa;
        font-size: 28px;
        font-weight: bold;
        position: absolute;
        right: 0;
        top: 0;
        width: 40px;
        text-align: center;
    }

    .close:hover,
    .close:focus {
        color: black;
        text-decoration: none;
        cursor: pointer;
    }*/
</style>
<!-- <div class="modal" id="productQuickView">
    <div class="modal-content">
        <span id="close" class="close">&times;</span>
        <form class="grid" id="form-quick-view">
            <div class="grid__item large--five-tenths">
                <div class="image-zoom">
                    <img id="p-product-image-feature" class="p-product-image-feature" src="#" alt="">
                    <div id="p-sliderproduct" class="flexslider">
                        <ul class="slides"></ul>
                    </div>
                </div>
            </div>
            <div class="grid__item large--five-tenths pull-right">
                <h4 class="p-title   modal-title " id="">Tên sản phẩm</h4>
                <p class="product-more-info">
                    <span class="product-sku">
                        Mã sản phẩm: <span id="ProductSku">01923123</span>
                    </span>
                </p>
                <div class="form-input product-price-wrapper">
                    <div class="product-price">
                        <span class="p-price "></span>
                        <del></del>
                    </div>
                    <em id="PriceSaving"></em>
                </div>
                <div class="p-option-wrapper">
                    <select name="id" class="" id="p-select-quickview"></select>
                </div>
                <div id="swatch-quick-view" class="select-swatch">
                </div>
                <div class="form-input hidden">
                    <label>Số lượng</label>
                    <input name="quantity" type="number" min="1" value="1" />
                </div>
                <div class="form-input" style="width: 100%">
                    <button type="submit" class="btn btn-addcart" id="AddToCardQuickView">Thêm vào giỏ</button>
                    <button disabled class="btn btn-soldout">Hết hàng</button>
                    <div class="qv-readmore">
                        <span> hoặc </span><a class="read-more p-url" href="#" role="button">Xem chi tiết</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div> -->

<?php wp_footer(); ?>
</body>
</html>