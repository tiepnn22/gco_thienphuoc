<?php get_header(); ?>

<?php
	$page_id       = get_the_ID();
	$page_name     = get_the_title();
	$page_content  = get_the_content(); //woo phải dùng the_content()

	//banner
    // $page_banner_check = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full');
    // $page_banner       = (!empty($page_banner_check[0])) ? $page_banner_check[0] : '';
    $data_page_banner  = array(
        // 'image_link'   =>    $page_banner,
        'image_alt'    =>    $page_name
    );
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">
        <section id="page-wrapper">
            <div class="wrapper">
                <div class="inner">
                    <h1><?php echo $page_name; ?></h1>
                    <div class="rte">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<?php get_footer(); ?>