<?php get_header(); ?>

<?php
	$tag_info 	= get_queried_object();
	$tag_id 	= $tag_info->term_id;
	$tag_name 	= $tag_info->name;

	//banner
	// $page_banner 	   = get_field('page_banner', 'category_'.$cat_id);
	$data_page_banner  = array(
		// 'image_link'   =>    $page_banner,
		'image_alt'    =>    $tag_name
	);
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">
        <section id="blog-wrapper">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">

                        <div class="grid__item large--nine-twelfths medium--one-whole small--one-whole pull-right">
                            <div class="blog-content">
                                <div class="blog-content-wrapper">
                                    <div class="blog-head">
                                        <div class="blog-title">
                                            <h1><?php echo $tag_name; ?></h1>
                                        </div>
                                    </div>

                                    <div class="blog-body">
                                        <div class="grid-uniform">

                                            <?php
                                                $query = query_post_by_tag_paged($tag_id, 4);
                                                $max_num_pages = $query->max_num_pages;

                                                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                                            ?>

                                                <?php get_template_part('resources/views/content/category-post', get_post_format()); ?>

                                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                                        </div>
                                    </div>

                                    <nav class="vk-pagination">
                                        <?php echo paginationCustom( $max_num_pages ); ?>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <?php get_sidebar(); ?>

                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<?php get_footer(); ?>