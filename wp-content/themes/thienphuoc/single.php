<?php get_header(); ?>

<?php
	$post_id = get_the_ID();

	$get_category = get_the_category($post_id);
	foreach ( $get_category as $get_category_kq ) {
	    $cat_id   = $get_category_kq->term_id;
	}
	$cat_name = get_cat_name($cat_id);
	
	//info post
	$single_post_title 		= get_the_title($post_id);
	$single_post_content 	= wpautop(get_the_content($post_id));
	$single_post_date 		= get_the_date('d/m/Y', $post_id);
	$single_post_link 		= get_permalink($post_id);
    $single_post_image 		= getPostImage($post_id,"full");
	$single_post_excerpt 	= cut_string(get_the_excerpt($post_id),300,'...');
	$single_recent_author 	= get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
	$single_post_author 	= $single_recent_author->display_name;
    $single_post_tag 		= get_the_tags($post_id);
    $post_comment       	= wp_count_comments($post_id);
    $post_comment_total 	= $post_comment->total_comments;

	//banner
	// $page_banner 	   = get_field('page_banner', 'category_'.$cat_id);
	$data_page_banner  = array(
		// 'image_link'   =>    $page_banner,
		'image_alt'    =>    $single_post_title
	);
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">
        <!-- <link rel="stylesheet" type="text/css" href="https://thanhnt7595.github.io/ArticleLayout/css/styles.css"> -->
        <!-- <link rel="stylesheet" type="text/css" href="https://thanhnt7595.github.io/ArticleLayout/css/partials/custom.css"> -->
        <section id="blog-wrapper">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">
                    	
                        <article class="grid__item large--nine-twelfths medium--one-whole small--one-whole">
                            <div class="article-content">
                                <div class="article-head">
                                    <div class="article-image">
                                        <img src="<?php echo $single_post_image; ?>" alt="<?php echo $single_post_title; ?>">
                                    </div>
                                    <h1><?php echo $single_post_title; ?></h1>
                                    <div class="grid mg-left-15">
                                        <div class="grid__item large--one-half medium--one-half small--one-whole pd-left-15">
                                            <div class="article-date-comment">
                                                <div class="date">
                                                	<i class="fa fa-calendar" aria-hidden="true"></i> 
                                                	<?php echo $single_post_date; ?>
                                                </div>
                                                <div class="comment">
                                                	<i class="fa fa-commenting-o" aria-hidden="true"></i> 
                                                	<span><?php echo $post_comment_total; ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="grid__item large--one-half medium--one-half small--one-whole pd-left-15">
                                            <div class="social-network-actions text-right">

												<?php get_template_part("resources/views/socical-bar"); ?>
												<div class="fb-like" data-href="<?php the_permalink(); ?>" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="true"></div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="article-body">
                                    <?php echo $single_post_content; ?>
                                </div>

                                <div class="article-tags clearfix">
                                    <span>Từ khóa:</span>
					                <?php
					                	if( !empty($single_post_tag) ) {
					                    foreach ($single_post_tag as $single_post_tag_kq) {

					                    $post_id = $single_post_tag_kq->term_id;
					                    $post_title = $single_post_tag_kq->name;
					                    $post_link = get_term_link($post_id);
					                ?>
					                	<a href="<?php echo $post_link; ?>">
			                            	<?php echo $post_title; ?>
		                            	</a>
		                            <?php } } ?>
                                </div>
                            </div>
                            
                            <div id="section-fbcomment" class="fb-comments-wrapper">
                                <!-- <div class="fb-comments" data-width="100%" data-href="https://suplo-noi-that.myharavan.com/blogs/news/nhung-tien-ich-uu-viet-cua-sofa-de-dap-ung-nhu-cau-nguoi-dung" data-numposts="5"></div> -->
                                <?php
                                    if ( comments_open() || get_comments_number() ) {
                                        comments_template();
                                    }
                                ?>
                            </div>

							<?php get_template_part("resources/views/template-related-post"); ?>

                        </article>

                        <?php get_sidebar();?>

                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<?php get_footer(); ?>