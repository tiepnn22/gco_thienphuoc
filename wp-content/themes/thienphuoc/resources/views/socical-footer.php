<?php
    //field
    $socical_phone          = get_field('socical_phone', 'option');
    $socical_zalo           = get_field('socical_zalo', 'option');
    $socical_messenger      = get_field('socical_messenger', 'option');
    $socical_chat_fb        = get_field('socical_chat_fb', 'option');
    $socical_back_to_top    = get_field('socical_back_to_top', 'option');   
?>

<!-- Chat fb -->
<?php if(!empty( $socical_chat_fb )) { echo $socical_chat_fb; } ?>



<!-- Socical -->
<div id="tool__society">
    <div class="tool__item">

        <?php if(!empty( $socical_phone )) { ?>
        <a href="tel:<?php echo $socical_phone; ?>" class="tool__icon tool__icon_tel" title="socical">
            <img src="<?php echo asset('images/icon/icon-phone.png'); ?>" alt="socical">
        </a>
        <?php } ?>

        <?php if(!empty( $socical_zalo )) { ?>
        <a href="https://zalo.me/<?php echo $socical_zalo; ?>" class="tool__icon tool__icon_zalo" title="socical" target="_blank">
            <img src="<?php echo asset('images/icon/icon-zalo.png'); ?>" alt="socical">
        </a>
        <?php } ?>

        <?php if(!empty( $socical_messenger )) { ?>
        <a href="https://<?php echo $socical_messenger; ?>" class="tool__icon tool__icon_mes" title="socical" target="_blank">
            <img src="<?php echo asset('images/icon/icon-mes.png'); ?>" alt="socical">
        </a>
        <?php } ?>

        <?php if(!empty( $socical_back_to_top )) { ?>
        <a href="javascript:void(0)" id="back-to-top" class="tool__icon tool__icon_back" title="back to top">
            <img src="<?php echo asset('images/icon/icon-back.png'); ?>" alt="socical">
        </a>
        <?php } ?>

    </div>
</div>

<style type="text/css">
    /*socical*/
    #tool__society {
        position: fixed;
        bottom: 45px;
        left: 10px;
        width: 48px;
        z-index: 9999;
    }
    #tool__society .tool__item {
        display: flex;
        display: -webkit-flex;
        flex-direction: column;
        -moz-flex-direction: column;
        -webkit-flex-direction: column;
        -o-flex-direction: column;
        -ms-flex-direction: column;
    }
    #tool__society .tool__item .tool__icon {
        display: flex;
        display: -webkit-flex;
        align-items: center;
        -moz-align-items: center;
        -webkit-align-items: center;
        -o-align-items: center;
        -ms-align-items: center;
        justify-content: center;
        -moz-justify-content: center;
        -webkit-justify-content: center;
        -o-justify-content: center;
        -ms-justify-content: center;
        width: 48px;
        height: 48px;
        margin: 20px 0 0;
        font-size: 30px;
        color: #fff;
        cursor: pointer;
        background: orange;
        border-radius: 50%;
        -moz-border-radius: 50%;
        -webkit-border-radius: 50%;
        -o-border-radius: 50%;
        -ms-border-radius: 50%;
        animation: zoom 1s infinite;
        -moz-animation: zoom 1s infinite;
        -webkit-animation: zoom 1s infinite;
        -o-animation: zoom 1s infinite;
        -ms-animation: zoom 1s infinite;
        animation-direction: alternate-reverse;
        -moz-animation-direction: alternate-reverse;
        -webkit-animation-direction: alternate-reverse;
        -o-animation-direction: alternate-reverse;
        -ms-animation-direction: alternate-reverse;
    }
    #tool__society .tool__item .tool__icon img {
        max-width: 100%;
        max-height: 100%;
        object-fit: contain;
        width: calc(100% - 15px);
    }
    #tool__society .tool__item .tool__icon.tool__icon_tel {
        background: #21BD56;
    }
    #tool__society .tool__item .tool__icon.tool__icon_zalo {
        background: #2074C8;
    }
    #tool__society .tool__item .tool__icon.tool__icon_mes {
        background: #007FFF;
    }
    /*#tool__society .tool__item .tool__icon.tool__icon_back {
        padding: 9px;
        text-align: center;
    }*/
    @keyframes zoom{
        from {
            box-shadow: rgba(16, 128, 199, 0.21) 0px 0px 0px 0px, rgba(16, 128, 199, 0.12) 0px 0px 0px 0px;
            -moz-box-shadow: rgba(16, 128, 199, 0.21) 0px 0px 0px 0px, rgba(16, 128, 199, 0.12) 0px 0px 0px 0px;
            -webkit-box-shadow: rgba(16, 128, 199, 0.21) 0px 0px 0px 0px, rgba(16, 128, 199, 0.12) 0px 0px 0px 0px;
            -o-box-shadow: rgba(16, 128, 199, 0.21) 0px 0px 0px 0px, rgba(16, 128, 199, 0.12) 0px 0px 0px 0px;
            -ms-box-shadow: rgba(16, 128, 199, 0.21) 0px 0px 0px 0px, rgba(16, 128, 199, 0.12) 0px 0px 0px 0px;
        }
        to {
            box-shadow: rgba(16, 128, 199, 0.21) 0px 0px 0px 5px, rgba(16, 128, 199, 0.12) 0px 0px 0px 10px;
            -moz-box-shadow: rgba(16, 128, 199, 0.21) 0px 0px 0px 5px, rgba(16, 128, 199, 0.12) 0px 0px 0px 10px;
            -webkit-box-shadow: rgba(16, 128, 199, 0.21) 0px 0px 0px 5px, rgba(16, 128, 199, 0.12) 0px 0px 0px 10px;
            -o-box-shadow: rgba(16, 128, 199, 0.21) 0px 0px 0px 5px, rgba(16, 128, 199, 0.12) 0px 0px 0px 10px;
            -ms-box-shadow: rgba(16, 128, 199, 0.21) 0px 0px 0px 5px, rgba(16, 128, 199, 0.12) 0px 0px 0px 10px;
        }
    }
    /*@media screen and (max-width: 480px) {
        #tool__society .tool__item {
            flex-direction: row;
            -moz-flex-direction: row;
            -webkit-flex-direction: row;
            -o-flex-direction: row;
            -ms-flex-direction: row;
        }
    }*/
</style>

<?php if(!empty( $socical_back_to_top )) { ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        // Back to top
        if (jQuery('#back-to-top').length) {
            jQuery("#back-to-top").on('click', function() {
                jQuery('html, body').animate({
                    scrollTop: jQuery('html, body').offset().top,
                }, 1000);
            });
        }
    });
</script>
<?php } ?>



<!-- Popup Mobile -->
<div class="popup-mobile">
    <div class='popup-content'>

        <div class="search-box">
            <form action="">
                <input type="text" class="form-control" required="required" placeholder="Tìm kiếm" name="s" value="">
                <button type="submit" class="btn search-icon">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>

    </div>
    <div class='popup-plush'><i class='fa fa-plus' aria-hidden='true'></i></div>
</div>

<style type="text/css">
    /*popup-mobile*/
    .popup-mobile {
        bottom: 50px;
        display: block;
        position: fixed;
        right: 90px;
        z-index: 99;
    }
    .popup-mobile .popup-content {
        display: none;
    }
    .popup-mobile .popup-content .search-box {
        display: block;
        width: auto;
        background: green;
        padding: 10px;
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        -o-border-radius: 5px;
        -ms-border-radius: 5px;
    }
    .popup-mobile .popup-content .search-box form {
        width: auto;
        display: block;
    }
    .popup-mobile .popup-content .search-box form input {
        height: 40px;
        background: #fff;
    }
    .popup-mobile .popup-content .search-box form .search-icon .fa {
        width: auto;
        padding: 5px 10px;
    }
    .popup-mobile .popup-plush {
        background: green none repeat scroll 0 0;
        color: #fff;
        cursor: pointer;
        float: right;
        font-size: 20px;
        line-height: 40px;
        height: 40px;
        margin-top: 10px;
        text-align: center;
        width: 40px;
    }
    .popup-mobile .fa {
        transition: all .3s;
        -moz-transition: all .3s;
        -webkit-transition: all .3s;
        -o-transition: all .3s;
        -ms-transition: all .3s;
    }
    .popup-mobile .active .fa {
        transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
    }
    /*@media screen and (max-width: 575px) {
        .popup-mobile {
            display: block;
        }
    }*/
</style>
<script type="text/javascript">
    jQuery(document).ready(function(){
        // Popup Mobile
        jQuery('.popup-plush').click(function() {
            jQuery(this).parent().find('.popup-content').stop(true, false).slideToggle();
            jQuery(this).toggleClass('active');
        });
    });
</script>