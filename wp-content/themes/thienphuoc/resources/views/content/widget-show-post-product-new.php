<?php
	$post_id 			= get_the_ID();
	$post_title 		= get_the_title($post_id);
	$post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-product");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),300,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
?>

<div class="featured-item">
    <div class="grid ">
        <div class="grid__item large--one-third medium--one-third small--one-third">
            <a href="<?php echo $post_link; ?>" class="featured-image">
                <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
            </a>
        </div>
        <div class="grid__item large--two-thirds medium--two-thirds small--two-thirds">
            <div class="featured-info">
                <a href="<?php echo $post_link; ?>" class="featured-title">
                    <?php echo $post_title; ?>
                </a>
                <?php echo show_price_old_price($post_id); ?>
            </div>
        </div>
    </div>
</div>