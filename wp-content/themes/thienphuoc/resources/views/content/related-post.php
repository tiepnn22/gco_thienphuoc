<?php
    $post_id            = get_the_ID();
    $post_title         = get_the_title($post_id);
    $post_content       = wpautop(get_the_content($post_id));
    $post_date          = get_the_date('d/m/Y',$post_id);
    $post_link          = get_permalink($post_id);
    $post_image         = getPostImage($post_id,"p-post");
    $post_excerpt       = cut_string(get_the_excerpt($post_id),200,'...');
    $post_author        = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
    $post_tag           = get_the_tags($post_id);
    $post_comment       = wp_count_comments($post_id);
    $post_comment_total = $post_comment->total_comments;
?>

<li>
    <a href="<?php echo $post_link; ?>" class="a">
        <?php echo $post_title; ?>
    </a>
</li>