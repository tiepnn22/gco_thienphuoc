<?php
	$post_id 			= get_the_ID();
	$post_title 		= get_the_title($post_id);
	$post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-product");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),300,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
?>

<div class="item grid__item pd-left-15 md-pd-left10">
    <div class="product-item">
        <div class="product-img">
            <a href="<?php echo $post_link; ?>">
                <img class="lazyload" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" />
                <div class="product-overlay"></div>
            </a>
            <div class="product-actions">
                <div class="quick-view btnQuickview medium--hide small--hide">
                	<?php echo do_shortcode('[woosq id="'.$post_id.'"]'); ?>
                </div>
                <div class="btnAddToCart medium--hide small--hide">
                	<?php echo show_add_to_cart_button($post_id); ?>
                </div>
                <!-- <div class="btnBuyNow medium--hide small--hide">Mua ngay</div> -->
            </div>
            <?php echo show_sale($post_id); ?>
        </div>
        <div class="product-info">
            <a href="<?php echo $post_link; ?>" class="product-title">
            	<?php echo $post_title; ?>
            </a>
            <?php echo show_price_old_price($post_id); ?>
        </div>
    </div>
</div>