<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	$post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-post");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
    $post_comment       = wp_count_comments($post_id);
    $post_comment_total = $post_comment->total_comments;
?>

<div class="grid__item large--one-half medium--one-half small--one-whole">
    <div class="article-item">
        <a class="article-image" href="<?php echo $post_link; ?>">
            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
        </a>
        <div class="article-info">
            <a href="<?php echo $post_link; ?>" class="article-title">
            	<?php echo $post_title; ?>
            </a>
            <div class="article-related">
                <div class="article-date">
                    <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $post_date; ?>
                </div>
                <div class="article-author">
                    <i class="fa fa-user" aria-hidden="true"></i> <?php echo $post_author; ?>
                </div>
                <div class="article-comment">
                    <i class="fa fa-commenting" aria-hidden="true"></i> <span><?php echo $post_comment_total; ?></span>
                </div>
            </div>
            <div class="article-desc">
                <?php echo $post_excerpt; ?>
            </div>
            <a href="<?php echo $post_link; ?>" class="article-view-more">[ Xem thêm ... ]</a>
        </div>
    </div>
</div>