<?php
	// $glb_ctp_product biến toàn cục
	// $glb_tax_product biến toàn cục
	global $post;
	$terms 		= get_the_terms( $post->ID , 'product_cat' , 'string');
	$term_ids 	= wp_list_pluck($terms,'term_id');
	
	$query = new WP_Query( array(
		'post_type' 	 => 'product',
		'tax_query' 	 => array(
			array(
				'taxonomy' 	=> 'product_cat',
				'field' 	=> 'id',
				'terms' 	=> $term_ids,
				'operator'	=> 'IN'
			 )),
		'posts_per_page' => -1,
		'orderby' 		 => 'date',
		'post__not_in'	 => array($post->ID)
	) );
?>

<div class="product-wrapper-related mg-bottom-30">
    <h2 class="section-title">
        Sản phẩm liên quan
    </h2>
    <div class="grid-uniform mg-left-15 md-mg-left-10">
        <div id="related-product-slider" class="owl--carousel owl-theme">

			<?php
				if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
			?>

				<?php get_template_part('resources/views/content/related-product', get_post_format()); ?>

			<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>
    </div>
</div>
