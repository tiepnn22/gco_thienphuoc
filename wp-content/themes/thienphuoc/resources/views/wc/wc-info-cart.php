<?php
    //cart
    $info_cart     = WC()->cart->cart_contents;
    $cart_count    = WC()->cart->get_cart_contents_count();
    $cart_total    = WC()->cart->get_cart_total();
    $cart_page_url = wc_get_cart_url();

    //checkout
    $checkout_page_url = wc_get_checkout_url();
?>

<div class="desktop-cart-wrapper">
    <a href="javascript:void(0)" class="hd-cart">
        <i class="fa fa-shopping-cart"></i>
        <span class="cart-count"><?php if($cart_count > 0) { echo $cart_count; } else { echo 0; } ?></span>
    </a>
    <div class="quickview-cart text-left">
        <ul class="minicart">
            <?php if($cart_count > 0) { ?>

                <?php
                    foreach ($info_cart as $info_cart_kq) {
// echo '<pre>';
// var_dump($info_cart_kq);
// echo '</pre>';
                    $product_id         = $info_cart_kq["product_id"];
                    $product_title      = get_the_title($product_id);
                    $product_link       = get_permalink($product_id);
                    $product_image      = getPostImage($product_id,"p-product");

                    $product_quantity   = $info_cart_kq["quantity"];
                    $line_total = $info_cart_kq["line_total"];
                    $product_price = $line_total / $product_quantity;
                ?>

                    <li class="single-cart-item">
                        <div class="cart-img">
                            <a title="<?php echo $product_title; ?>" href="<?php echo $product_link; ?>">
                                <img src="<?php echo $product_image; ?>" alt="<?php echo $product_title; ?>" />
                            </a>
                        </div>
                        <div class="cart-content">
                            <h3>
                                <a title="<?php echo $product_title; ?>" href="<?php echo $product_link; ?>">
                                    <?php echo $product_title; ?>
                                </a>
                            </h3>
                            <?php echo format_price_donvi($product_price); ?>
                        </div>
                        <div class="del-icon trash" data-productid="<?php echo $product_id; ?>">
                            <a title href="javascript:void(0)">
                                x <?php echo $product_quantity; ?>
                            </a>
                        </div>
                    </li>

                <?php } ?>

                <li>
                    <div class="total-price">
                        <span class="f-left">
                            <?php _e('Tổng', 'text_domain'); ?>:
                        </span>
                        <span class="f-right">
                            <?php echo $cart_total; ?>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="checkout-link">
                        <a title="Giỏ hàng" href="<?php echo $cart_page_url; ?>">
                            <?php _e('Giỏ hàng', 'text_domain'); ?>
                        </a>
                        <a title="Thanh toán" href="<?php echo $checkout_page_url; ?>">
                            <?php _e('Thanh toán', 'text_domain'); ?>
                        </a>
                    </div>
                </li>

            <?php } else { ?>
                <?php _e('Giỏ hàng trống !', 'text_domain'); ?>
            <?php } ?>
        </ul>
    </div>
</div>