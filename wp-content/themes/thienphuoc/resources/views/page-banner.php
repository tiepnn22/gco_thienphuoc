<?php
	global $data_page_banner;
	
	if(!empty( $data_page_banner )) {
		// $image_link_check = $data_page_banner['image_link'];
		// $image_link = (!empty($image_link_check)) ? $image_link_check : asset('images/2x1.png');
		$image_alt 	= $data_page_banner['image_alt'];
	}
?>

<section id="breadcrumb-wrapper5" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
    <div class="breadcrumb-content">
        <div class="wrapper">
            <div class="inner text-center">
                <div class="breadcrumb-big">
                    <h2>
                        <?php echo $image_alt;?>
                    </h2>
                </div>
                <div class="breadcrumb-small">
			        <?php
			            if(function_exists('bcn_display')) { 
			                echo '<a href="' . site_url() . '">Trang chủ </a> /';
			                bcn_display(); 
			            }
			        ?>
                </div>
            </div>
        </div>
    </div>
</section>