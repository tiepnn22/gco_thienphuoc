<form action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="wpo-search">
        <div class="wpo-search-inner">
            <div class="input-group">

                <input type="text" class="form-control input-search" required="required" placeholder="<?php _e('Tìm kiếm...', 'text_domain'); ?>" name="s" value="<?php echo get_search_query(); ?>">

                <span class="input-group-btn">
                    <button type="submit">
                    	<i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </span>

            </div>
        </div>
    </div>
</form>