<?php
class show_list_category extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_list_category',
            'Core - Hiển thị danh sách category',
            array( 'description'  =>  'Hiển thị danh sách category' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hiển thị danh sách category',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);

        echo '<p>';
            echo 'Tiêu đề :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/>';
        echo '</p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $categories = get_categories( array(
            'orderby' => 'name',
            'parent'  => 0
        ) );

        echo $before_widget; ?>
        <div class="list-categories">
            <div class="blog-sb-title clearfix">
                <h3>
                    <?php echo $title; ?>
                </h3>
            </div>
            <ul class="no-bullets">

                <?php
                    foreach ( $categories as $foreach_kq ) {
                  
                    $post_id = $foreach_kq->term_id;
                    $post_title = $foreach_kq->name;
                    $post_link = get_term_link($post_id);
                ?>
                    <li class="">
                        <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <?php echo $after_widget;
    }
}
function create_showlistcategory_widget() {
    register_widget('show_list_category');
}
add_action( 'widgets_init', 'create_showlistcategory_widget' );
?>