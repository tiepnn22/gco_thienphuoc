<?php get_header(); ?>

<h1 style="display: none;"><?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?></h1>

<?php
    //field
    $home_popup_image     = get_field('home_popup_image');
    $home_popup_content   = get_field('home_popup_content');
    $home_popup_form_id   = get_field('home_popup_form');
    $home_popup_form      = do_shortcode('[contact-form-7 id="'.$home_popup_form_id.'"]');

    $home_slide = get_field('home_slide');

    $home_product_select_cat = get_field('home_product_select_cat');

    $home_product_sale_title        = get_field('home_product_sale_title');
    $home_product_sale_date         = get_field('home_product_sale_date');
    $home_product_sale_select_post  = get_field('home_product_sale_select_post');

    $home_service_title         = get_field('home_service_title');
    $home_service_desc          = get_field('home_service_desc');
    $home_service_select_post   = get_field('home_service_select_post');

    $home_gallery_collection = get_field('home_gallery_collection');

    $home_testimonial = get_field('home_testimonial');

    $home_achievement = get_field('home_achievement');

    $home_product_shop_title    = get_field('home_product_shop_title');
    $home_product_shop_desc     = get_field('home_product_shop_desc');

    $home_desc_one_title    = get_field('home_desc_one_title');
    $home_desc_one_desc     = get_field('home_desc_one_desc');
    $home_desc_one_url      = get_field('home_desc_one_url');
    $home_desc_two_title    = get_field('home_desc_two_title');
    $home_desc_two_desc     = get_field('home_desc_two_desc');
    $home_desc_two_url      = get_field('home_desc_two_url');
    $home_desc_three_title  = get_field('home_desc_three_title');
    $home_desc_three_form_id    = get_field('home_desc_three_form');
    $home_desc_three_form       = do_shortcode('[contact-form-7 id="'.$home_desc_three_form_id.'"]');

    $home_news_title        = get_field('home_news_title');
    $home_news_select_post  = get_field('home_news_select_post');

    $home_partner_title = get_field('home_partner_title');
    $home_partner_desc  = get_field('home_partner_desc');
    $home_partner       = get_field('home_partner');
?>

<?php if(!empty( $home_slide )) { ?>
<section id="home-main-slider">
    <div id="main-slider" class="owl-carousel owl-theme">

        <?php
            foreach ($home_slide as $foreach_kq) {

            $post_image = $foreach_kq["image"];
            $post_title = $foreach_kq["title"];
            $post_link  = $foreach_kq["url"];
        ?>
            <div class="item">
                <div class="main-slider-item">
                    <a href="collections/all.html">
                        <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" />
                    </a>
                    <div class="collection-info clearfix">
                        <div class="text-center">
                            <a href="<?php echo $post_link; ?>" class="collection-title"><?php echo $post_title; ?></a>
                            <a href="<?php echo $post_link; ?>" class="btn-1">Xem thêm</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

    </div>
</section>
<?php } ?>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">

        <?php if(!empty( $home_product_select_cat )) { ?>
        <section id="home-list-collection">
            <div class="wrapper">
                <div id="list-collection-slider" class="owl-carousel owl-theme">

                    <?php
                        foreach ($home_product_select_cat as $foreach_kq) {

                        $term_id        = $foreach_kq->term_id;
                        $term_desc      = cut_string( $foreach_kq->description ,300,'...');
                        $taxonomy_slug  = $foreach_kq->taxonomy;
                        $term_name      = get_term( $term_id, $taxonomy_slug )->name;
                        $term_link      = get_term_link(get_term( $term_id ));
                        $thumbnail_id   = get_term_meta( $term_id, 'thumbnail_id', true );
                        $term_image     = wp_get_attachment_url( $thumbnail_id ); // woo
                        $term_image_icon     = get_field('category_product_icon', 'category_'.$term_id);
                    ?>
                        <div class="item">
                            <div class="collection-item">
                                <div class="collection-image">
                                    <img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>">
                                    <div class="overlay"></div>
                                </div>
                                <div class="collection-icon clearfix">
                                    <a href="<?php echo $term_link; ?>">
                                        <img src="<?php echo $term_image_icon; ?>" alt="<?php echo $term_name; ?>">
                                        <?php echo $term_name; ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </section>
        <?php } ?>

        <section id="home-collection-sale">
            <div class="wrapper">
                <div class="inner">
                    <h2 class="section-title"><?php echo $home_product_sale_title; ?></h2>
                    <div class="home-countdown text-center">
                        <div class="countdown-days">
                            <div id="days">0</div>
                            <div>Ngày</div>
                        </div>
                        <div class="countdown-hrs">
                            <div id="hrs">0</div>
                            <div>Giờ</div>
                        </div>
                        <div class="countdown-mins">
                            <div id="mins">0</div>
                            <div>Phút</div>
                        </div>
                        <div class="countdown-secs">
                            <div id="secs">0</div>
                            <div>Giây</div>
                        </div>
                    </div>

                    <?php if(!empty( $home_product_sale_select_post )) { ?>
                    <div class="grid md-mg-left-10">
                        <div id="home-collection-sale-slide" class="owl-carousel owl-theme">

                            <?php if(!empty( $home_product_sale_select_post )) { ?>
                            <?php
                                foreach ($home_product_sale_select_post as $foreach_kq) {

                                $post_id            = $foreach_kq->ID;
                                $post_title         = get_the_title($post_id);
                                $post_content       = wpautop(get_the_content($post_id));
                                $post_date          = get_the_date('d/m/Y',$post_id);
                                $post_link          = get_permalink($post_id);
                                $post_image         = getPostImage($post_id,"p-product");
                                $post_excerpt       = cut_string(get_the_excerpt($post_id),200,'...');
                                $post_author        = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                                $post_tag           = get_the_tags($post_id);
                            ?>
                                <div class="item grid__item md-pd-left10">
                                    <div class="product-item">
                                        <div class="product-img">
                                            <a href="<?php echo $post_link; ?>">
                                                <img class="lazyload" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" />
                                                <div class="product-overlay"></div>
                                            </a>
                                            <div class="product-actions">
                                                <div class="quick-view btnQuickview medium--hide small--hide">
                                                    <?php echo do_shortcode('[woosq id="'.$post_id.'"]'); ?>
                                                </div>
                                                <div class="btnAddToCart medium--hide small--hide">
                                                    <?php echo show_add_to_cart_button($post_id); ?>
                                                </div>
                                                <!-- <div class="btnBuyNow medium--hide small--hide">Mua ngay</div> -->
                                            </div>
                                            <?php echo show_sale($post_id); ?>
                                        </div>
                                        <div class="product-info">
                                            <a href="<?php echo $post_link; ?>" class="product-title">
                                                <?php echo $post_title; ?>
                                            </a>
                                            <?php echo show_price_old_price($post_id); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php } ?>

                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </section>

        <section id="home-article-1">
            <div class="wrapper">
                <div class="inner">
                    <h2 class="section-title"><?php echo $home_service_title; ?></h2>
                    <div class="section-desc"><?php echo wpautop($home_service_desc); ?></div>

                    <?php if(!empty( $home_service_select_post )) { ?>
                    <div class="grid">
                        <div id="home-article-slider" class="owl-carousel owl-theme">

                            <?php
                                foreach ($home_service_select_post as $foreach_kq) {

                                $post_id            = $foreach_kq->ID;
                                $post_title         = get_the_title($post_id);
                                $post_date          = get_the_date('d/m/Y',$post_id);
                                $post_link          = get_permalink($post_id);
                                $post_image         = getPostImage($post_id,"p-post");
                                $post_excerpt       = cut_string(get_the_excerpt($post_id),200,'...');
                                $post_author        = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                                $post_tag           = get_the_tags($post_id);
                                $post_comment       = wp_count_comments($post_id);
                                $post_comment_total = $post_comment->total_comments;
                            ?>
                                <div class="item">
                                    <div class="grid__item">
                                        <div class="article-item">
                                            <a class="article-image" href="<?php echo $post_link; ?>">
                                                <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                                            </a>
                                            <div class="article-info">
                                                <a href="<?php echo $post_link; ?>" class="article-title">
                                                    <?php echo $post_title; ?>
                                                </a>
                                                <div class="article-related">
                                                    <div class="article-date">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $post_date; ?>
                                                    </div>
                                                    <div class="article-author">
                                                        <i class="fa fa-user" aria-hidden="true"></i> <?php echo $post_author; ?>
                                                    </div>
                                                    <div class="article-comment">
                                                        <i class="fa fa-commenting" aria-hidden="true"></i> <span><?php echo $post_comment_total; ?></span>
                                                    </div>
                                                </div>
                                                <div class="article-desc">
                                                    <?php echo $post_excerpt; ?>
                                                </div>
                                                <a href="<?php echo $post_link; ?>" class="article-view-more">[ Xem thêm ... ]</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </section>

        <section id="home-collection-1">
            <div id="home-collection1-slider" class="owl-carousel owl-theme">

                <?php if(!empty( $home_gallery_collection )) { ?>
                <?php
                    $y = 1;
                    foreach ($home_gallery_collection as $foreach_kq) {

                    $post_title     = $foreach_kq["title"];
                    $post_gallery   = $foreach_kq["gallery"];
                    // var_dump($post_gallery);
                ?>
                    <div class="item">
                        <div class="product-item2">

                            <?php if(!empty( $post_gallery )) { ?>
                            <?php
                                $i = 1;
                                foreach ($post_gallery as $foreach_kq) {

                                $post_image = $foreach_kq;
                            ?>
                                <a href="<?php echo $post_image; ?>" class="<?php if($i == 1) { echo 'product-image'; } else { echo 'hide'; } ?>" data-fancybox="home-gallery-images-<?php echo $y; ?>" data-caption="">
                                    <img class="lazyload" src="" data-src="<?php echo $post_image; ?>" alt="" />
                                    <?php if($i == 1) { ?>
                                        <div class="overlay"></div>
                                        <div class="product-info">
                                            <span class="product-title"><?php echo $post_title; ?></span>
                                        </div>
                                    <?php } ?>
                                </a>
                            <?php $i++; } ?>
                            <?php } ?>

                        </div>
                    </div>
                <?php $y++; } ?>
                <?php } ?>

            </div>
        </section>

        <section id="home-review">
            <div class="review-overlay">

                <?php if(!empty( $home_testimonial )) { ?>
                <div id="home-review-slider" class="owl-carousel owl-theme">

                    <?php
                        foreach ($home_testimonial as $foreach_kq) {

                        $post_image = $foreach_kq["image"];
                        $post_title = $foreach_kq["title"];
                        $post_desc  = $foreach_kq["desc"];
                    ?>
                        <div class="item">
                            <div class="review-item">
                                <p class="review-content">
                                    <?php echo $post_desc; ?>
                                </p>
                                <div class="review-user">
                                    <img class="lazyload" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                                    <div class="user-name"><?php echo $post_title; ?></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
                <?php } ?>

                <?php if(!empty( $home_achievement )) { ?>
                <div class="hr-statistic text-center">
                    <div class="wrapper">
                        <div class="grid-uniform">

                            <?php
                                foreach ($home_achievement as $foreach_kq) {

                                $post_icon = $foreach_kq["icon"];
                                $post_title = $foreach_kq["title"];
                                $post_number  = $foreach_kq["number"];
                            ?>
                                <div class="grid__item large--one-quarter medium--one-half small--one-whole mg-bottom-30">
                                    <div class="hr-statistic-icon">
                                        <?php echo $post_icon; ?>
                                    </div>
                                    <div class="hr-statistic-number">
                                        <span data-count="1999"><?php echo $post_number; ?></span>
                                    </div>
                                    <div class="hr-statistic-text">
                                        <?php echo $post_title; ?>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <?php } ?>

            </div>
        </section>

        <section id="home-collection-2">
            <div class="wrapper">
                <div class="inner">
                    <h2 class="section-title"><?php echo $home_product_shop_title; ?></h2>
                    <div class="section-desc"><?php echo $home_product_shop_desc; ?></div>
                    <div class="tab ">
                        <button class="tablinks" onclick="openTab(event, 'Sản phẩm nổi bật')" id="defaultOpen">Sản phẩm nổi bật
                        </button>
                        <button class="tablinks" onclick="openTab(event, 'Sản phẩm mới')">Sản phẩm mới
                        </button>
                    </div>
                    <div id="Sản phẩm nổi bật" class="tabcontent">
                        <div class="grid-uniform md-mg-left-10">

                            <?php
                                $tax_query[] = array(
                                    'taxonomy' => 'product_visibility',
                                    'field'    => 'name',
                                    'terms'    => 'featured',
                                    'operator' => 'IN',
                                );
                                $query =  new WP_Query( array(
                                    'post_type'             => 'product',
                                    'post_status'           => 'publish',
                                    'ignore_sticky_posts'   => 1,
                                    'showposts'             => 8,
                                    'orderby'               => 'date',
                                    'order'                 => 'DESC',
                                    'tax_query'             => $tax_query
                                ) );
                                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                            ?>

                                <?php get_template_part('resources/views/content/home-product-tab', get_post_format()); ?>

                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                        </div>
                    </div>
                    <div id="Sản phẩm mới" class="tabcontent">
                        <div class="grid-uniform md-mg-left-10">

                            <?php
                                $query = query_post_by_custompost('product', 8);
                                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                            ?>

                                <?php get_template_part('resources/views/content/home-product-tab', get_post_format()); ?>

                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="home-subscribe">
            <div class="subscribe-ovelay">
            </div>
            <div class="grid mg-left-0 subscribe-content ">
                <div class="grid__item large--one-third pd-left-15">
                    <div class="banner-item">
                        <div class="banner-inner">
                            <div class="banner-title"><?php echo $home_desc_one_title; ?></div>
                            <div class="banner-desc"><?php echo $home_desc_one_desc; ?></div>
                            <a href="<?php echo $home_desc_one_url; ?>" class="banner-link">Xem ngay</a>
                        </div>
                    </div>
                </div>
                <div class="grid__item large--one-third pd-left-15">
                    <div class="banner-item">
                        <div class="banner-inner">
                            <div class="banner-title"><?php echo $home_desc_two_title; ?></div>
                            <div class="banner-desc"><?php echo $home_desc_two_desc; ?></div>
                            <a href="<?php echo $home_desc_two_url; ?>" class="banner-link">Tham khảo</a>
                        </div>
                    </div>
                </div>
                <div class="grid__item large--one-third pd-left-15">
                    <div class="banner-item">
                        <div class="banner-inner">
                            <div class="banner-title"><?php echo $home_desc_three_title; ?></div>

                            <?php if(!empty( $home_desc_three_form )) { ?>
                            <div class='contact-form'>
                                <?php echo $home_desc_three_form; ?>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="home-article-2">
            <div class="wrapper">
                <div class="inner">
                    <h2 class="section-title">
                        <?php echo $home_news_title; ?>
                    </h2>
                    <div class="grid">

                        <?php if(!empty( $home_news_select_post )) { ?>
                        <?php
                            $i = 1;
                            foreach ($home_news_select_post as $foreach_kq) {

                            $post_id            = $foreach_kq->ID;
                            $post_title         = get_the_title($post_id);
                            $post_date          = get_the_date('d/m/Y',$post_id);
                            $post_link          = get_permalink($post_id);
                            $post_image         = getPostImage($post_id,"p-post");
                            $post_excerpt       = cut_string(get_the_excerpt($post_id),200,'...');
                            $post_author        = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                            $post_tag           = get_the_tags($post_id);
                            $post_comment       = wp_count_comments($post_id);
                            $post_comment_total = $post_comment->total_comments;

                            if($i == 1) {
                        ?>
                            <div class="grid__item large--one-half medium--one-whole small--one-whole article-first">
                                <div class="article-item2">
                                    <a href="<?php echo $post_link; ?>" class="article-image">
                                        <img class="lazyload" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                                        <div class="article-overlay"></div>
                                    </a>
                                    <div class="article-info">
                                        <a href="<?php echo $post_link; ?>" class="article-title">
                                            <?php echo $post_title; ?>
                                        </a>
                                        <div class="article-date">
                                            <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $post_date; ?>
                                        </div>
                                        <div class="article-author">
                                            <i class="fa fa-user" aria-hidden="true"></i><span> <?php echo $post_author; ?></span>
                                        </div>
                                        <div class="article-comment">
                                            <i class="fa fa-commenting" aria-hidden="true"></i> <span><?php echo $post_comment_total; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } $i++; } ?>
                        <?php } ?>

                        <div class="grid__item large--one-half medium--one-whole small--one-whole">
                            <div class="home-article-right">
                                <div class="grid">
                                    
                                    <?php if(!empty( $home_news_select_post )) { ?>
                                    <?php
                                        $i = 1;
                                        foreach ($home_news_select_post as $foreach_kq) {

                                        $post_id            = $foreach_kq->ID;
                                        $post_title         = get_the_title($post_id);
                                        $post_date          = get_the_date('d/m/Y',$post_id);
                                        $post_link          = get_permalink($post_id);
                                        $post_image         = getPostImage($post_id,"p-post");
                                        $post_excerpt       = cut_string(get_the_excerpt($post_id),200,'...');
                                        $post_author        = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                                        $post_tag           = get_the_tags($post_id);
                                        $post_comment       = wp_count_comments($post_id);
                                        $post_comment_total = $post_comment->total_comments;

                                        if($i > 1) {
                                            if($i > 3) { echo '<div class="list-article">'; }
                                    ?>
                                            <div class="grid__item <?php if($i < 4) { echo 'large--one-half medium--one-half small--one-whole'; } ?>">
                                                <div class="article-item2">
                                                    <a href="<?php echo $post_link; ?>" class="article-image">
                                                        <img class="lazyload" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                                                        <div class="article-overlay"></div>
                                                    </a>
                                                    <div class="article-info">
                                                        <a href="<?php echo $post_link; ?>" class="article-title">
                                                            <?php echo $post_title; ?>
                                                        </a>
                                                        <div class="article-date">
                                                            <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $post_date; ?>
                                                        </div>
                                                        <div class="article-author">
                                                            <i class="fa fa-user" aria-hidden="true"></i><span> <?php echo $post_author; ?></span>
                                                        </div>
                                                        <div class="article-comment">
                                                            <i class="fa fa-commenting" aria-hidden="true"></i> <span><?php echo $post_comment_total; ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php
                                            if($i > 3) { echo '</div>'; }
                                        } $i++; } ?>
                                    <?php } ?>

                                    <!-- <div class="grid__item large--one-half medium--one-half small--one-whole">
                                        <div class="article-item2">
                                            <a href="blogs/news/huong-dan-dat-sofa-theo-phong-thuy-theo-phong-cach-tay-au-1.html" class="article-image">
                                                <img class="lazyload" src="http://theme.hstatic.net/1000256920/1000358817/14/lazyload.jpg?v=317" data-src="http://file.hstatic.net/1000256920/article/blog_6_047f65b9c04d4453a7f7f44839ce2a57_large.jpg" alt="Hướng dẫn đặt sofa theo phong thủy theo phong cách Tây Âu">
                                                <div class="article-overlay">
                                                </div>
                                            </a>
                                            <div class="article-info">
                                                <a href="blogs/news/huong-dan-dat-sofa-theo-phong-thuy-theo-phong-cach-tay-au-1.html" class="article-title">Hướng dẫn đặt sofa theo phong thủy theo phong cách Tây Âu</a>
                                                <div class="article-date">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i> 12/01/18
                                                </div>
                                                <div class="article-author">
                                                    <i class="fa fa-user" aria-hidden="true"></i><span> Suplo Noi That</span>
                                                </div>
                                                <div class="article-comment">
                                                    <i class="fa fa-commenting" aria-hidden="true"></i> <span class="fb-comments-count" data-href="blogs/news/huong-dan-dat-sofa-theo-phong-thuy-theo-phong-cach-tay-au-1.html"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid__item large--one-half medium--one-half small--one-whole">
                                        <div class="article-item2">
                                            <a href="blogs/news/ghe-sofa-kich-thuoc-be-giup-nguoi-dung-co-the-de-nhung-noi-hep.html" class="article-image">
                                                <img class="lazyload" src="http://theme.hstatic.net/1000256920/1000358817/14/lazyload.jpg?v=317" data-src="http://file.hstatic.net/1000256920/article/blog_5_7be85d04b8c94bc29eb1b4715bcd6f58_large.jpg" alt="Ghế sofa kích thước bé giúp người dùng có thể để những nơi hẹp">
                                                <div class="article-overlay">
                                                </div>
                                            </a>
                                            <div class="article-info">
                                                <a href="blogs/news/ghe-sofa-kich-thuoc-be-giup-nguoi-dung-co-the-de-nhung-noi-hep.html" class="article-title">Ghế sofa kích thước bé giúp người dùng có thể để những nơi hẹp</a>
                                                <div class="article-date">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i> 12/01/18
                                                </div>
                                                <div class="article-author">
                                                    <i class="fa fa-user" aria-hidden="true"></i><span> Suplo Noi That</span>
                                                </div>
                                                <div class="article-comment">
                                                    <i class="fa fa-commenting" aria-hidden="true"></i> <span class="fb-comments-count" data-href="blogs/news/ghe-sofa-kich-thuoc-be-giup-nguoi-dung-co-the-de-nhung-noi-hep.html"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="list-article">
                                        <div class="grid__item ">
                                            <div class="article-item2">
                                                <a href="blogs/news/y-tuong-thiet-ke-mau-sac-trong-can-phong-trung-tinh.html" class="article-image">
                                                    <img class="lazyload" src="http://theme.hstatic.net/1000256920/1000358817/14/lazyload.jpg?v=317" data-src="http://file.hstatic.net/1000256920/article/noi-that-3-1481715114_large.jpg" alt="Ý tưởng thiết kế màu sắc trong căn phòng trung tính">
                                                    <div class="article-overlay">
                                                    </div>
                                                </a>
                                                <div class="article-info">
                                                    <a href="blogs/news/y-tuong-thiet-ke-mau-sac-trong-can-phong-trung-tinh.html" class="article-title">Ý tưởng thiết kế màu sắc trong căn phòng trung tính</a>
                                                    <div class="article-date">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i> 12/01/18
                                                    </div>
                                                    <div class="article-author">
                                                        <i class="fa fa-user" aria-hidden="true"></i><span> Suplo Noi That</span>
                                                    </div>
                                                    <div class="article-comment">
                                                        <i class="fa fa-commenting" aria-hidden="true"></i> <span class="fb-comments-count" data-href="blogs/news/y-tuong-thiet-ke-mau-sac-trong-can-phong-trung-tinh.html"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="home-partners">
            <div class="wrapper">
                <div class="inner">
                    <h2 class="section-title">
                        <?php echo $home_partner_title; ?>
                    </h2>
                    <div class="section-desc">
                        <?php echo $home_partner_desc; ?>
                    </div>

                    <?php if(!empty( $home_partner )) { ?>
                    <div class="grid">
                        <div id="home-partners-slider" class="owl-carousel owl-theme">

                            <?php
                                foreach ($home_partner as $foreach_kq) {

                                $post_image = $foreach_kq["image"];
                                $post_link  = $foreach_kq["url"];
                            ?>
                                <div class="item grid__item">
                                    <div class="partner-item">
                                        <a href="<?php echo $post_link; ?>">
                                            <img src="<?php echo $post_image; ?>" alt="">
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </section>

    </main>
</div>

<!-- popup -->
<section>
<div id="popup-subscribe" class="popup">
    <div id="popup-modal" class="popup-content  animate down" style="background-image: url('<?php echo $home_popup_image; ?>');">
        <span class="close-popup"><i class="fa fa-times" aria-hidden="true"></i></span>
        <div class="grid">
            <div class="grid__item push--large--one-half large--six-twelfths medium--one-whole small--one-whole pull-left">
                <div class="popup-wrapper">
                    <div class="popup-list">
                        <?php echo $home_popup_content; ?>
                    </div>
                    <div class="popup-form">

                        <?php if(!empty( $home_popup_form )) { ?>
                        <div class='contact-form'>
                            <?php echo $home_popup_form; ?>
                        </div>
                        <?php } ?>

                        <div class="popup-form">
                            <a href="facebook.com/suploteam.html" class="popup-social-network" target="_blank">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                            <a href="instargram.html" class="popup-social-network" target="_blank">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                            <a href="twitter.html" class="popup-social-network" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                            <a href="youtube.html" class="popup-social-network" target="_blank">
                                <i class="fa fa-youtube" aria-hidden="true"></i>
                            </a>
                            <a href="googleplus.html" class="popup-social-network" target="_blank">
                                <i class="fa fa-google-plus" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</section>
<script>
    var modal = document.getElementById('popup-subscribe');
    var span = document.getElementsByClassName("close-popup")[0];

    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onload = function(event) {
        modal.style.display = "block";
    }
</script>

<!-- countDownDate sale product -->
<script>
    // Set the date we're counting down to
    var countDownDate = new Date("<?php echo $home_product_sale_date; ?>").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        if ( days > 0 ) { document.getElementById("days").innerHTML = days; }
        if ( hours > 0 ) { document.getElementById("hrs").innerHTML = hours; }
        if ( minutes > 0 ) { document.getElementById("mins").innerHTML = minutes; }
        if ( seconds > 0 ) { document.getElementById("secs").innerHTML = seconds; }

        // If the count down is finished, write some text
        //if (distance < 0) {
        //      clearInterval(x);
        //    document.getElementById("flash-sale-status").innerHTML = "Flash Sale đã kết thúc";
        //}
    }, 1000);
</script>

<?php get_footer(); ?>

