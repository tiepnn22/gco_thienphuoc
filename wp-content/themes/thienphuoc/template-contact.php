<?php
	/*
	Template Name: Mẫu Liên hệ
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //banner
    // $page_banner_check = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full');
    // $page_banner       = (!empty($page_banner_check[0])) ? $page_banner_check[0] : '';
    $data_page_banner  = array(
        // 'image_link'   =>    $page_banner,
        'image_alt'    =>    $page_name
    );

    //field
    $customer_address   = get_field('customer_address', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_email     = get_field('customer_email', 'option');

    $contact_contact_form_id    = get_field('contact_contact_form');
    $contact_contact_form       = do_shortcode('[contact-form-7 id="'.$contact_contact_form_id.'"]');

    $contact_map      = get_field('contact_map');
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">
        <section id="page-wrapper">
            <div class="wrapper">
                <div class="inner">

                    <div class="contact-map"><?php echo $contact_map; ?></div>

                    <div class="page-contact">

                        <h1><?php echo $page_name; ?></h1>

                        <div class="grid">
                            <div class="grid__item large--four-tenths">
                                <div class="contact-content">
                                    <div class="contact-item">
                                        <h3 class="contact-title hotline-title">Số điện thoại/Hotline</h3>
                                        <div class="contact-icon">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>" class="contact-info">
                                                <?php echo $customer_phone; ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="contact-item">
                                        <h3 class="contact-title address-title">Địa chỉ</h3>
                                        <div class="contact-icon">
                                            <i class="fa fa-home" aria-hidden="true"></i>
                                            <p class="contact-info">
                                                <?php echo $customer_address; ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="contact-item">
                                        <h3 class="contact-title email-title">Email:</h3>
                                        <div class="contact-icon">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <a href="mailto:<?php echo $customer_email; ?>" class="contact-info">
                                                <?php echo $customer_email; ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="grid__item large--six-tenths">
                                <div class="contact-form">
                                    <div class="form-vertical clearfix">
                                        <?php if(!empty( $contact_contact_form )) { ?>
                                        <div class='contact-form'>
                                            <?php echo $contact_contact_form; ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                </div>
            </div>
        </section>
    </main>
</div>

<?php get_footer(); ?>