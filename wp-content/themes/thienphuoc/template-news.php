<?php
	/*
	Template Name: Mẫu Tin tức
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //banner
    // $page_banner_check = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full');
    // $page_banner       = (!empty($page_banner_check[0])) ? $page_banner_check[0] : '';
    $data_page_banner  = array(
        // 'image_link'   =>    $page_banner,
        'image_alt'    =>    $page_name
    );
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class=" main-content" role="main">
        <section id="blog-wrapper">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">

                        <div class="grid__item large--nine-twelfths medium--one-whole small--one-whole pull-right">
                            <div class="blog-content">
                                <div class="blog-content-wrapper">
                                    <div class="blog-head">
                                        <div class="blog-title">
                                            <h1><?php echo $page_name; ?></h1>
                                        </div>
                                    </div>

                                    <div class="blog-body">
                                        <div class="grid-uniform">

                                            <?php
                                                $query = query_post_by_custompost_paged('post', 4);
                                                $max_num_pages = $query->max_num_pages;

                                                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                                            ?>

                                                <?php get_template_part('resources/views/content/category-post', get_post_format()); ?>

                                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                                        </div>
                                    </div>

                                    <nav class="vk-pagination">
                                        <?php echo paginationCustom( $max_num_pages ); ?>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <?php get_sidebar(); ?>

                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<?php get_footer(); ?>