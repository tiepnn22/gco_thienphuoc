<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="canonical" href="<?php echo get_page_link_current(); ?>" />
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>

    <!-- <link href='http://theme.hstatic.net/1000256920/1000358817/14/timber.scss.css?v=317' rel='stylesheet' type='text/css' media='all' /> -->
    <!-- <link href='http://theme.hstatic.net/1000256920/1000358817/14/suplo-style.scss.css?v=317' rel='stylesheet' type='text/css' media='all' /> -->
    <!-- <link href='http://theme.hstatic.net/1000256920/1000358817/14/owl.carousel.css?v=317' rel='stylesheet' type='text/css' media='all' /> -->
    <!-- <link href='http://theme.hstatic.net/1000256920/1000358817/14/owl.theme.css?v=317' rel='stylesheet' type='text/css' media='all' /> -->
    <!-- <link href='http://theme.hstatic.net/1000256920/1000358817/14/owl.transitions.css?v=317' rel='stylesheet' type='text/css' media='all' /> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Encode+Sans+Condensed:400,700&amp;subset=vietnamese" rel="stylesheet"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&amp;subset=vietnamese" rel="stylesheet"> -->
    <!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->

<!--     <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/timber.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/suplo.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/carousel-theme.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/carousel-transitions.css">

    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/font-encode.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/font-montserrat.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/css/font-awesome/css/font-awesome.min.css">

    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/modernizr.min.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/jquery_lazyload.js"></script> -->

    <!-- <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js' type='text/javascript'></script> -->
    <!-- <script src='http://theme.hstatic.net/1000256920/1000358817/14/modernizr.min.js?v=317' type='text/javascript'></script> -->
    <!-- <script src="https://cdn.rawgit.com/tuupola/jquery_lazyload/0a5e0785a90eb41a6411d67a2f2e56d55bbecbd3/lazyload.js"></script> -->
    
    <script type="text/javascript" charset="utf-8">
        window.addEventListener("load", function(event) {
            lazyload();
        });
    </script>
    <script>
        // window.file_url = "http://file.hstatic.net/1000256920/file/"; 
        // window.asset_url = "http://theme.hstatic.net/1000256920/1000358817/14/?v=317";
        // var check_variant = true;
        // var check_variant_quickview = true;
    </script>
</head>

<body <?php body_class(); ?>>
<!-- class="template-index"  -->

<?php
    //field
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_email     = get_field('customer_email', 'option');

    $f_socical_facebook   = get_field('f_socical_facebook', 'option');
    $f_socical_insta      = get_field('f_socical_insta', 'option');
    $f_socical_youtube    = get_field('f_socical_youtube', 'option');
    $f_socical_twitter    = get_field('f_socical_twitter', 'option');
    $f_socical_google     = get_field('f_socical_google', 'option');
?>

<header id="header">

    <!-- header pc-->
    <div class="header-desktop">

        <?php if(!is_front_page()) { ?>
        <div class="header-top-nohome medium--hide small--hide">
            <div class="wrapper">
                <div class="inner">
                    <div class="header-top">
                        <div class="grid">
                            <div class="grid__item large--one-half">
                                <div class="header-top-info">
                                    <a href="<?php echo get_link_page_template( 'template-store.php' ); ?>" class="header-shop-map">
                                        <i class="fa fa-map-marker"></i>Hệ thống cửa hàng
                                    </a>
                                    <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>">
                                        <i class="fa fa-phone"></i> <?php echo $customer_phone; ?>
                                    </a>
                                    <a href="mailto:<?php echo $customer_email; ?>">
                                        <i class="fa fa-envelope"></i> <?php echo $customer_email; ?>
                                    </a>
                                </div>
                            </div>
                            <div class="grid__item large--one-half">
                                <div class="header-top-social">
                                    <a href="<?php echo $f_socical_facebook; ?>" target="_blank">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                    <a href="<?php echo $f_socical_insta; ?>" target="_blank">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                    <a href="<?php echo $f_socical_youtube; ?>" target="_blank">
                                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                    </a>
                                    <a href="<?php echo $f_socical_twitter; ?>" target="_blank">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                    <a href="<?php echo $f_socical_google; ?>" target="_blank">
                                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        
        <div class="<?php if(!is_front_page()) { echo 'header-navbar'; } else { echo 'header-index'; } ?> medium--hide small--hide">
            <div class="wrapper">
                <div class="inner">
                    <nav class="nav-bar <?php if(!is_front_page()) { echo 'nav-product'; } ?>">
                        <div class="grid">

                            <!-- logo -->
                            <div class="grid__item large--two-twelfths">
                                <div class="header-logo">
                                    <?php get_template_part("resources/views/logo"); ?>
                                </div>
                            </div>

                            <div class="grid__item large--ten-twelfths text-right">

                                <!-- menu pc-->
                                <?php get_template_part("resources/views/menu"); ?>

                                <!-- account -->
                                <div class="header-account">
                                    <a href="javascript:void(0)">Tài khoản</a>
                                    <ul class="no-bullets list-actions text-left">
                                        <li>
                                            <a href="<?php echo wp_login_url(); ?>">Đăng nhập</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('/wp-login.php?action=register'); ?>">Đăng ký</a>
                                        </li>
                                    </ul>
                                </div>
                            
                                <!-- search -->
                                <div class="hc-search-wrapper">
                                    <a id="show_search_smart" href="javascript:void(0)"><i class="fa fa-search"></i></a>
                                    <div class="search-form-wrapper" data-id="im here">
                                        <div id="searchauto">

                                            <?php get_template_part("resources/views/search-form"); ?>

                                        </div>
                                    </div>
                                </div>

                                <!-- cart pc -->
                                <div class="header-cart">
                                    <?php get_template_part("resources/views/wc/wc-info-cart"); ?>
                                </div>

                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <!-- header mobile -->
    <div class=" mobile-header header-navbar large--hide">
        <div class="grid">

            <!-- logo mobile -->
            <div class="grid__item medium--one-third small--one-half">
                <div class="hd-logo text-left">
                    <?php get_template_part("resources/views/logo"); ?>
                </div>
            </div>

            <div class="grid__item large--two-twelfths push--large--eight-twelfths medium--two-thirds small--one-half clearfix text-right">
                <div class="hd-btnMenu">
                    <a href="#menu" data-menu="#menu" data class="icon-fallback-text site-nav__link js-drawer-open-right">
                        <span>Menu</span>
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </a>
                </div>

                <!-- cart mobile -->
                <?php get_template_part("resources/views/wc/wc-info-cart"); ?>

            </div>
        </div>
    </div>

</header>

<!--menu mobile-->
<nav class="d-lg-none" id="menu">
    <?php get_template_part("resources/views/menu"); ?>
</nav>
